<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_calls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('super_agent_id');
            $table->unsignedInteger('agent_id');
            $table->decimal('amount');
            $table->string('agent_phone');
            $table->string('status');
            $table->string('reference');
            $table->text('agent_location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_calls');
    }
}

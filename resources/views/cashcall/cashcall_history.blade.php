@extends('layouts.app')

@section('page-title', 'superagent')
{{--@section('page-heading', 'SUPER AGENT')--}}

@section('breadcrumbs')
    <li class="breadcrumb-item active">
        Cash Call Management &raquo; &nbsp;
        Cash Calls History
    </li>
@stop

@section('content')

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <div class="table-responsive" id="users-table-wrapper">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S/n</th>
                        <th >CashCall </th>
                        <th >Status</th>
                        <th >Date</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')

@stop

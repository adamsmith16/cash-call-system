@extends('layouts.app')

@section('page-title', 'superagent')
{{--@section('page-heading', 'SUPER AGENT')--}}

@section('breadcrumbs')
    <li class="breadcrumb-item active">
        Cash Call Management &raquo; &nbsp;
        Completed Cash Calls
    </li>
@stop

@section('content')

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <div class="table-responsive" id="users-table-wrapper">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S/n</th>
                        <th >Super Agent</th>
                        <th >Agent</th>
                        <th >Amount</th>
                        <th >Agent phone</th>
                        <th >Status</th>
                        <th >Reference</th>
                        <th >Agent Location</th>
                        <th >Date</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')

@stop

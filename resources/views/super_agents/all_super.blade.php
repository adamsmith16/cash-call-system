@extends('layouts.app')

@section('page-title', 'superagent')
{{--@section('page-heading', 'SUPER AGENT')--}}

@section('breadcrumbs')
    <li class="breadcrumb-item active">
        Super Agents &raquo; &nbsp;
        All Super Agents
    </li>
@stop

@section('content')

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <div class="table-responsive" id="users-table-wrapper">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S/n</th>
                        <th >Name</th>
                        <th >Total CashCall</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')

@stop

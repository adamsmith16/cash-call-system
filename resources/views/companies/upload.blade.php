@extends('layouts.app')

@section('page-title', trans('app.general_settings'))
@section('page-heading', 'Companies')

@section('breadcrumbs')
    <li class="breadcrumb-item text-muted">
        Companies
    </li>
    <li class="breadcrumb-item active">
        Upload Companies
    </li>
@stop

@section('content')
    @include('partials.messages')

    <div class="card">
        <div class="card-body">
                {!! Form::open(array('method' => 'post', 'route' => 'companies.import.excel', 'class' => 'form', 'files'=>true, 'enctype'=>'multipart/form-data')) !!}
                {{ csrf_field() }}
                    <div class=" ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <a href="{{route('companies.list')}}" aria-hidden="true">&times;</a>
                        </button>
                    </div>
                         <div class="col-md-6">
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="file" class="bold">Choose File</label>
                                    <input type="file" name="file_name" id="file_id" class="form-control" >
                                </div>
                            </div>
                         </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id = "submit" > {!! 'Upload' !!}</button>
                    </div>
                {{Form::close()}}
        </div>
    </div>
@stop

@extends('layouts.app')

@section('page-title', trans('app.general_settings'))
@section('page-heading', 'Companies')

@section('breadcrumbs')
    <li class="breadcrumb-item text-muted">
        Companies
    </li>
    <li class="breadcrumb-item active">
        List Companies
    </li>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive card">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>state</th>
                        <th>City</th>
                        <th>Zip Code</th>
                        <th>Lead Status</th>
                        <th>Date Added</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->company_name }}</td>
                            <td>{{ $company->address }}</td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->phone_number }}</td>
                            <td>{{ $company->companyState->state_name }}</td>
                            <td>{{ $company->companyCity->city }}</td>
                            <td>{{ $company->zip_code }}</td>
                            <td>{{ $company->lead_status }}</td>
                            <td>{{ $company->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

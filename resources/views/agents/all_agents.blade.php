@extends('layouts.app')

@section('page-title', 'superagent')
{{--@section('page-heading', 'SUPER AGENT')--}}

@section('breadcrumbs')
    <li class="breadcrumb-item active">
        Agents &raquo; &nbsp;
        All Agents
    </li>
@stop

@section('content')

    @include('partials.messages')

    <div class="card">
        <div class="card-body">

            <div class="table-responsive" id="users-table-wrapper">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>S/n</th>
                        <th >Agent Identifier</th>
                        <th >Location</th>
                        <th >Phone Number</th>
                        <th >Super Agent</th>
                        <th >Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('scripts')

@stop

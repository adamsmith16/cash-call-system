@extends('layouts.app')

@section('page-title', trans('app.general_settings'))
@section('page-heading', 'Schools')

@section('breadcrumbs')
    <li class="breadcrumb-item text-muted">
        School
    </li>
    <li class="breadcrumb-item active">
        Add Schools
    </li>
@stop

@section('content')
    @include('partials.messages')

    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'school.store', 'id' => 'school-form']) !!}
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="school_name" class="form-control-label"> School Name</label>
                    {!! Form::text('school_name','', array('placeholder' => '','class' => 'form-control','id'=>'school_name','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="email" class="form-control-label">Principal First Name</label>
                    {!! Form::text('principal_first_name','', array('placeholder' => '','class' => 'form-control','id'=>'principal_first_name','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="principal_last_name" class="form-control-label">Principal Last Name</label>
                    {!! Form::text('principal_last_name','', array('placeholder' => '','class' => 'form-control','id'=>'principal_first_name','required'=>'')) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="address" class="form-control-label">Address</label>
                    {!! Form::textarea('address','', array('placeholder' => '','class' => 'form-control', 'rows'=>'2', 'id'=>'address','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="state_id" class="form-control-label">state</label>
                    {!! Form::select('state_id',[''=>'Select State']+$states, null, array('class' => 'form-control c-select select2','id'=>'state_id','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="city_id" class="form-control-label">City</label>
                    {!! Form::select('city_id',[''=>''], null, array('class' => 'form-control c-select','id'=>'city_id','required'=>'')) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="zip_code" class="form-control-label">Zip Code</label>
                    {!! Form::text('zip_code','', array('placeholder' => '','class' => 'form-control','id'=>'zip_code','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="phone_number" class="form-control-label">Phone Number</label>
                    {!! Form::text('phone_number','', array('placeholder' => '','class' => 'form-control','id'=>'phone_number','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="fax" class="form-control-label">Fax </label>
                    {!! Form::text('fax','', array('placeholder' => '','class' => 'form-control','id'=>'fax','required'=>'')) !!}
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-4">
                    <label for="email" class="form-control-label"> email </label>
                    {!! Form::email('email','', array('placeholder' => '','class' => 'form-control','id'=>'email','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="title" class="form-control-label">Title</label>
                    {!! Form::text('title','', array('placeholder' => '','class' => 'form-control','id'=>'title','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="lead_status" class="form-control-label"> Lead Status </label>
                    {!! Form::text('lead_status','', array('placeholder' => '','class' => 'form-control','id'=>'lead_status','required'=>'')) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="lead_call_status" class="form-control-label">Lead Source</label>
                    {!! Form::text('lead_source','', array('placeholder' => '','class' => 'form-control','id'=>'lead_source','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="lead_call_status" class="form-control-label">Lead Call Status</label>
                    {!! Form::text('lead_call_status','', array('placeholder' => '','class' => 'form-control','id'=>'lead_call_status','required'=>'')) !!}
                </div>

                <div class="col-sm-4">
                    <label for="lead_priority" class="form-control-label">Lead Priority</label>
                    {!! Form::text('lead_priority','', array('placeholder' => '','class' => 'form-control','id'=>'lead_priority','required'=>'')) !!}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="gfs_type" class="form-control-label">Gfs Type</label>
                    {!! Form::text('gfs_type','', array('placeholder' => '','class' => 'form-control','id'=>'gfs_type','required'=>'')) !!}
                </div>
            </div>

            <div class="form-group row m-t-md">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary m-t"> submit </button>
                </div>
            </div>
        {{Form::close()}}
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('#state_id').change(function () {
            // var arr = {};
            var stateID = $(this).val();
            if(stateID){
                console.log(stateID)
                $.ajax({
                    type:"GET",
                    url: "cities/"+stateID,
                    success:function (result) {
                        if(result){
                            console.log(result)
                            // arr = result
                            $("#city_id").empty();
                            $("#city_id").append('<option> Select City </option>')
                            $.each(result,function (key,value) {
                                $('#city_id').append('<option value = "'+key+'">'+value+'</option>');
                            });
                        }else{
                            $("#city_id").empty();
                        }
                    }
                });
            }
        });

        $('.select2').se
    </script>
@stop

@extends('layouts.app')

@section('page-title', trans('app.general_settings'))
@section('page-heading', 'School')

@section('breadcrumbs')
    <li class="breadcrumb-item text-muted">
        Schools
    </li>
    <li class="breadcrumb-item active">
        List Schools
    </li>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive card">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>School Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>state</th>
                        <th>City</th>
                        <th>Zip Code</th>
                        <th>Lead Status</th>
                        <th>Date Added</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($schools as $school)
                        <tr>
                            <td>{{ $school->school_name }}</td>
                            <td>{{ $school->address }}</td>
                            <td>{{ $school->email }}</td>
                            <td>{{ $school->phone_number }}</td>
                            <td>{{ $school->schoolState->state_name }}</td>
                            <td>{{ $school->schoolCity->city }}</td>
                            <td>{{ $school->zip_code }}</td>
                            <td>{{ $school->lead_status }}</td>
                            <td>{{ $school->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

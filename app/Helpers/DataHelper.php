<?php
/**
 * Created by PhpStorm.
 * User: adamsmith
 * Date: 1/21/20
 * Time: 4:44 PM
 */

namespace Vanguard\Helpers;

use Vanguard\States;


class DataHelper
{
    public static function StatesArray()
    {
        $array = States::select('states.state_name', 'states.id')->pluck('states.state_name', 'states.id')->toArray();
        return $array;
    }

}

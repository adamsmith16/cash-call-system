<?php
/**
 * Created by PhpStorm.
 * User: adamsmith
 * Date: 1/22/20
 * Time: 1:25 PM
 */

namespace Vanguard\Http\Requests\Company;


use Vanguard\Http\Requests\Request;

class CreateCompanyRequest extends Request
{

    /**
     * @return array
     */
    public function rules(){
        $rules = [
            'company_name'=> 'required',
            'contact_first_name' => 'required',
            'contact_last_name' => 'required',
            'address' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'zip_code' => 'required',
            'phone_number' => 'required',
            'fax' => 'required ',
            'email' => 'required|email|unique:schools,email',
            'title' => 'required',
            'lead_source' => 'required',
            'lead_status'=> 'required',
            'lead_call_status' => 'required',
            'gfs_type' => 'required',
            'lead_priority' => 'required',
        ];

        return $rules;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: adamsmith
 * Date: 1/22/20
 * Time: 9:55 AM
 */

namespace Vanguard\Http\Requests\School;


use Vanguard\Http\Requests\Request;

class CreateSchoolRequest extends Request
{

    /**
     * @return array
     */
    public function rules(){
        $rules = [
            'school_name'=> 'required',
            'principal_first_name' => 'required',
            'principal_last_name' => 'required',
            'address' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'zip_code' => 'required',
            'phone_number' => 'required',
            'fax' => 'required ',
            'email' => 'required|email|unique:schools,email',
            'title' => 'required',
            'lead_source' => 'required',
            'lead_status'=> 'required',
            'lead_call_status' => 'required',
            'gfs_type' => 'required',
            'lead_priority' => 'required',
        ];

        return $rules;
    }
}

<?php

namespace Vanguard\Http\Controllers\Web;

use Illuminate\Http\Request;
use Vanguard\Http\Controllers\Controller;


class AgentController extends Controller
{
    public function allAgents(){
        return view('agents.all_agents');
    }
}

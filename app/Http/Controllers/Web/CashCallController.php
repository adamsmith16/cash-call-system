<?php

namespace Vanguard\Http\Controllers\Web;

use Illuminate\Http\Request;
use Vanguard\Http\Controllers\Controller;


class CashCallController extends Controller
{
    public function allCashCall(){
        return view('cashcall.all_cashcall');
    }

    public function pendingCashCall(){
        return view('cashcall.pending_cashcall');
    }

    public function completedCashCall(){
        return view('cashcall.completed_cashcall');
    }

    public function cashCallHistory(){
        return view('cashcall.cashcall_history');
    }

    public function cashCallLogs(){
        return view('cashcall.cashcall_logs');
    }
}
